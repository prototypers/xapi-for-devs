# TODO

## Bugs

- Path: dont flow nice when more than one line

## Features

- Indicate number of variants per node in list
- Add macros
- Front page?
- Update web engine guide

## Styling

- more similar to developer.webex: more padding on articles, more buttons instead of links, more air in articles, more space in menu

## Cleanup

- Dont filter nodes by name in App component, do it like the other filters

## Future

- Add blog, with welcome article


# Feedback / input for future tasks to Krank

- Many nodes for appear for different products without any visible difference (eg signage mode)
- Would be great to have existing help text for command parameters (already exists)
- Would be great to have events documented (already exists)
- Would be great to have external product names instead of internal (hopen, svea ...)


Blog
- Link in header
- Routes
- Skeleton without left menu
- Mock a few articles and meta data
- Blog list
- Article loader
- Blog article page
- "Welcome" blog


Macro examples
- Link in header
- Routes
- List page
- A few macros, meta data
- Macro details
  - Title, description
  - Screenshots
  - Links to file
  - Preview files


## Main feature blocks

The building blocks of our open platform:

* xAPI
The API that all integrations use to talk to the video device.

* Macros
Custom code snippets that run on the video device itself.

* User interface extensions
Custom panels, buttons and widgets to extend the user intefaces.

* Customer branding
Add your company's profile to the video devices

* Web apps
Web applications that are hosted in the cloud.

* JsxAPI
Node/JavaScript SDK for the xAPI.

* Cloud
Modern REST interface for the xAPI

* Http (legacy)
Legacy HTTP API

* 3rd party control systems
Drivers for industry standard control systems such as Crestron.

* Virtual sources
Extend the user interface to support external video switchers

* CEC
Control connected screens with CEC commands
