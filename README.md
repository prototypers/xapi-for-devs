# Webex devices for developers

The goal for this documentation tool is:

- Make xAPI reference docs available publicly online to 3rd party developers and integrators
- Make it easy to browse and search
- Make it easy/automatic to maintain (get auto generated data from CE and documentation from doc team)
- Make it look and feel similar to developer.webex.com
- Provide tools to easily copy/paste code for developers

## Technology stack

- Bitbucket public repo (so dont put anything secret in here)
- Node.js and Git
- React for client side UI code
- parcel.js for transpiling and bundling the code
- No server requirements, everything can be hosted statically

## xAPI schema files

This site uses the same xAPI schema files xapi.cisco.com serves to its user interface. To find one:

- Log in to xapi.cisco.com (Intranet)
- Select API Overview
- Open your developer console
- Select the network tab
- In Revision, search for the tag you want (eg ce-9.14.2) and click it
- In the networks tab, find the last api schema ("commit-api"), eg https://xapi.cisco.com/krank/commit-api/11374
- Right click and Open in new tab
- Save the json file as eg 9.14.2.json in your schemas/
- Add it to your schemas/schemas-public.json file

Ideally this process could be fully automatic, or at least be triggered by a single command on the command line.

## Local development

- Clone the repo
- do `npm install`
- do `npm start`
- Fetch the xapi schema files from xapi.cisco.com that you want
- Create your schemas.json file, with the following structure:
  [
    {
      "name": "ce-9.13.0",
      "url": "ce-9.13.0.json"
    }
  ]
 (add more schemas if you want. make sure you have valid json, no dangling commas, use " not ')

- Copy the schemas.json file and the schema files to dist/schemas/
- Copy the whole markdown folder to the dist/schemas/ folder as well
- Load localhost:1234/ in your browser, it should show the xapis you have downloaded

The raw schema files from xapi.cisco.com contain private APIs and should never be added to git.

## Publishing / updating the public/private site

- Run ./deploy.sh to build the site, copy the schema files and deploy the site to public and private (internal) xapi sites

## schemas.json file

The schemas file contains all the xapi schemas you want available on the site (9.13, 9.15, ...)
It is not part of Git.

Syntax of schemas-public.json is a list of name and url for schema:

```
[
  { "name": "9.14", "url": "9.14.json" },
  { "name": "9.13", "url": "9.13.json" },
  { "name": "9.12", "url": "9.12.json" }
]
```

## Hosting the tool

It is very straight forward to host the generated site. The router used is reactrouter.com with hash router (eg http://kyberheimen.com/xapi/#/path/Call), this means you don't need to deal with pesky nginx redirect rules or subdomains on the server to make it work. Just copy and paste the content of the build/ folder to your chosen web server.
