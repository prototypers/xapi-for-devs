# User Interface Extensions

User Interface Extensions allow you to create new panels and action buttons on the Webex Devices to create custom behaviour. This can be used to simplify the workflow, automate tasks on the video device or control entirely different devices in the room, such as lights, blinds, air condition, etc.

The UI Extensions editor is available on the web interface on each device, but you can also try the editor here and export your configuration, if you wish:

<a class="button" href="https://custom-collab.cisco.com/uieditor/">Try editor</a>

<a href="https://custom-collab.cisco.com/uieditor/" target="_blank">
  <img src="./markdown/images/ui-extensions-editor.png" />
</a>
