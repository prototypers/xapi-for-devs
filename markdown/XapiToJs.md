# Mapping from xAPI to JavaScript

Do you now how to use the command line and are familiar with the xAPI already? Below you will find examples for each API use case. They all assume that you have imported the `xapi` object.

Also remember that you can find generated code snippets for every API node in the XAPI reference.

## Configurations

Get value:

```
// Command line:
xConfiguration Audio DefaultVolume

// Js:
xapi.Config.Audio.DefaultVolume.get().then(v => console.log(v))
```

Set value:

```
// Command line:
xConfiguration Audio DefaultVolume: 44

// Js:
xapi.Config.Audio.DefaultVolume.set(44)
```

Register feedback:

```
// Command line:
xFeedback register Configuration/Audio/DefaultVolume

// Js:
xapi.Config.Audio.DefaultVolume.on(v => console.log(v))
```

### Status

Get value:

```
// Command line:
xStatus Audio Volume

// Js:
xapi.Status.Audio.Volume.get().then(v => console.log(v))
```

Register feedback:

```
// Command line:
xFeedback register Status/Audio/Volume

// Js:
xapi.Status.Audio.Volume.on(v => console.log(v))
```

### Commands

Invoke command:

```
// Command line:
xCommand Audio Volume Set Level: 33

// Js:
xapi.Command.Audio.Volume.Set({ Level: 24 });
```

Multi-line command:

```
// Command line (with ending dot):
xCommand UserInterface Branding Upload Type: Background
data:image/png;base64,iVBORw0KGgoAAA...
.

// Js:
xapi.Command.UserInterface.Branding.Upload
    ({ Type: 'Background' }, 'data:image/png;base64,iVBORw0KGgoAAA...')
```

Multiple arguments with the same name:

```
// Command line:
xCommand Presentation Start ConnectorId: 1 ConnectorId: 2

// Js:
xapi.Command.Presentation.Start({ ConnectorId: [1, 2] });
```

Use result from command:

```
// Command line:
xCommand Phonebook Search PhonebookType: Local

// Js:
xapi.Command.Phonebook.Search({ PhonebookType: "Local" })
  .then((result) => console.log(result))
```

### Events

Subscribe to events:

```
// Command line:
xFeedback Register Event/UserInterface/Extensions/Widget/Action

// Js:
xapi.Event.UserInterface.Extensions.Widget.Action
  .on((event) => console.log(event))
```

### Stopping feedback

To stop a feedback listener for configurations, statuses or events, follow this pattern:

```
// Command line:
xFeedback Register Event/Standby
...
xFeedback Deregister Event/Standby

// Js:
const stop = xapi.Event.Standby.on(event => console.log(event));
// ...
stop();
```
