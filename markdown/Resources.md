# Resources

<table class="links">
  <thead>
    <tr>
      <th>Link</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://cs.co/roomdevices" target="_blank">DevNet</a></td>
      <td>Official developer site for Room devices, including labs, sandboxes etc</td>
    </tr>
    <tr>
      <td><a href="https://www.cisco.com/c/en/us/support/collaboration-endpoints/spark-room-kit-series/products-command-reference-list.html" target="_blank">Original xAPI docs</a></td>
      <td>Original CE xAPI reference documentation (PDF).</td>
    </tr>
    <tr>
      <td><a href="https://www.npmjs.com/package/jsxapi" target="_blank">JSXAPI</a></td>
      <td>JavaScript SDK for server (Node) and browser</td></tr>
    <tr>
      <td>
        <a href="https://projectworkplace.cisco.com/" target="_blank">Project Workplace</a>
        </td>
        <td>Overview of the Webex Room devices</td>
    </tr>
    <tr>
      <td>
        <a href="https://www.cisco.com/c/en/us/support/collaboration-endpoints/telepresence-quick-set-series/products-installation-and-configuration-guides-list.html" target="_blank">Configuration guides</a>
      </td>
      <td>Official Cisco guides to customize the Room devices</td>
    </tr>
    <tr>
      <td>
        <a href="https://github.com/CiscoDevNet/roomdevices-macros-samples/" target="_blank">Macro examples</a>
      </td>
      <td>Git repository with relevant macro examples.</td>
    </tr>
    <tr>
      <td>
        <a href="https://developer.webex.com/" target="_blank">Webex for developers</a>
      </td>
      <td>Reference documentation for Webex APIs.</td>
    </tr>
    <tr>
      <td>
        <a href="https://github.com/voipnorm/CE-Deploy" target="_blank">CE Deploy</a>
      </td>
      <td>Tool to mass deploy customisations to Webex Room devices.</td>
    </tr>
  </tbody>
</table>
