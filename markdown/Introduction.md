# The xAPIs

> xAPI is the API used by the Webex Room devices. As a developer or integrator, you can create extensions, automate tasks, get metrics to analyse usage and much, much more.

<img src="./markdown/images/roomkit.jpg" />

All Cisco Webex Room devices support the xAPI. Since different devices, such as the Webex Board, the Desk Pro and the Webex Panorama has very different hardware and feature set, the xAPI is different for each device. But the core features of the xAPI remain the same across all of them.

For more xAPI documentation, training and labs, see the Cisco DevNet pages: <a href="https://cs.co/roomdevices">cs.co/roomdevices</a>
