
function findDomains(nodeList) {
  const list = [];
  nodeList.forEach((node) => {
    const domain = node.normPath.split(' ')[0];
    list[domain] = true;
  });

  const domains = Object.keys(list);
  return domains;
}

function findProducts(nodeList) {
  const list = new Set();
  nodeList.forEach((node) => {
    node.products.forEach(p => list.add(p));
  });
  return Array.from(list);
}

function findNodesInDomain(nodes, domain) {
  const list = nodes.filter((node) => {
    const top = node.normPath.split(' ')[0];
    return top === domain;
  });
  return list;
}

function findProductNodes(nodes, product) {
  if (product === 'all') return [].concat(nodes);

  return nodes.filter((node) => {
    return node.products.find(p => p === product);
  });
}

function hasFilter(filters, name) {
  return filters[name] && filters[name] != 'all' ? filters[name] : false;
}

function filter(nodes, filters) {
  // console.log('filter', filters);
  const { Type } = filters;
  let result = nodes;

  const type = hasFilter(filters, 'Type');
  if (type) {
    result = result.filter(i => i.type === type);
  }

  const deployment = hasFilter(filters, 'Deployment');
  if (deployment) {
    result = result.filter(i => i.attributes.backend === 'any' || i.attributes.backend === deployment);
  }

  const visibility = hasFilter(filters, 'Visibility');
  if (visibility) {
    result = result.filter(i => i.attributes.access === visibility);
  }

  const user = hasFilter(filters, 'User');
  if (user) {
    result = result.filter(i => i.attributes.role.find(j => j === user));
  }

  return result;
}

function containsInternal(nodes) {
  return nodes.some(n => n.attributes.access === 'internal');
}

function isVariant(node1, node2) {
  if (!node1 || !node2) {
    return false;
  }
  return node1.path === node2.path && node1.type === node2.type;
}

function removeVariants(nodes) {
  const unique = [];
  let previous;
  nodes.forEach((node) => {
    if (!isVariant(node, previous)) {
      unique.push(node);
    }
    previous = node;
  });
  return unique;
}

function textSearch(nodes, word, fullText = true) {

  const contains = (needle, haystack) => {
    if (!needle || !haystack) return false;
    return haystack.match(needle);
  };

  return nodes.filter((node) => {
    const attr = node.attributes || {};
    const params = (attr.params || []).join(' ');
    const fromWildcard = word.replace(/\*/g, '.*'); // so users can type *, not .*
    const pattern = new RegExp(fromWildcard, 'i');

    if (contains(pattern, node.path)) return true;
    if (!fullText) return; // only search path name
    if (contains(pattern, attr.description)) return true;
    if (node.type === 'Command') {
      if (attr.params.some(p => contains(pattern, p.name))) return true;
      if (attr.params.some(p => contains(pattern, p.description))) return true;
    }
    if (attr.valuespace && attr.valuespace.Values) {
      if (attr.valuespace.Values.some(v => contains(pattern, v))) return true;
    }
    return false;
  });
}

module.exports = {
  findDomains, filter, findNodesInDomain, findProducts, findProductNodes, textSearch,
  removeVariants, containsInternal,
};
