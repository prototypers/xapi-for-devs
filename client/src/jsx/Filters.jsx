const React = require('react');

const { useRouteMatch, useHistory } = require('react-router-dom');

const { productGroups } = require('../productHelper');

/*
Filtes:
- search word
- type (config, cmd, ...)
- version (9.14)
- deployment (cloud, onprem)
- role (integrator, ...)
- visibility (public, internal)
- device (desk pro, ...)
*/

const SearchButton = (props) => {
  const { keyword } = props;
  let history = useHistory();
  const search = () => {
    if (keyword.length < 3) {
      alert('Keyword must be at least 3 characters')
    }
    else {
      history.push(`/search/${keyword}`);
    }
  }

  return (
    <button className="primary" onClick={search}>Go</button>
  );
};

const FilterBox = (props) => {
  if (props.name === 'Product') {
    const options = productGroups.map(({ name, products }) => {
      const list = products.map((v) => (
        <option key={v.key} value={v.key}>{v.name}</option>
      ));
      return (
        <optgroup key={name} label={name}>
          {list}
        </optgroup>
      );
    });
    options.unshift(<option key="all" value="all">Any product</option>);

    return <SelectBox {...props} groupedValues={options} />;
  }
  return <SelectBox {...props} />;
}

const SelectBox = (props) => {
  const { onChange, values, value, name, active, groupedValues} = props;
  const choices = groupedValues || values.map(v => <option key={v.key} value={v.key}>{v.name}</option>);
  const set = e => onChange(name, e.target.value);
  const cls = active ? 'active' : '';

  return (
    <div className="filter">
      <select className={cls} onChange={set} value={value}>
        {choices}
      </select>
    </div>
  );
};


const Filters = (props) => {
  const { setOption, filters = [], options, schema, product } = props;
  const [keyword, setKeyword] = React.useState('');

  // console.log(props);
  const list = filters.map((f) => {
    const { name, values } = f;
    let value;
    if (name === 'Schema') value = schema;
    else if (name === 'Product') value = product;
    else value = options[name];
    const active = value && !value.startsWith('all');

    return (
      <FilterBox
        active={active}
        key={name}
        name={name}
        values={values}
        onChange={setOption}
        value={value}
      />
    );
  });

  const updateKeyword = e => setKeyword(e.target.value);
  const match = useRouteMatch('/search/:keyword');
  const word = !keyword && match ? match.params.keyword : keyword;

  return (
    <form className="filterSettings">
      <input placeholder="Search xAPI" value={word} onChange={updateKeyword} />
      <SearchButton keyword={word}/>
      {list}
    </form>
  );
}

module.exports = Filters;
