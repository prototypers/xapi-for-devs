const React = require('react');

const XapiBasics = () => {
  return (
    <content className="article">
      <h1>xAPI</h1>
      <div>
      The xAPI is consists of four building main building blocks:
        <ul>
          <li>
            <label className="label Command">Commands</label>
            The most common way to interact with the video system, such as making a call, starting a presentation or changing the mic volume. The commands usually lead to temporary changes, not kept after reboot. Typically a command causes a status value to change. Commands can accept parameters (eg who to call and the call rate).
          </li>
          <li>
          <label className="label Status">Status</label>
            Properties in the video system that may change over time. You can get values on request, or get notified when they change. Examples: the current system volume, whether the system is in a call or whether someone is presenting.
          </li>
          <li>
            <label className="label Configuration">Configs</label>
            Permanent settings on the video system such as system name, default volume, proximity mode, wallpaper image etc. These settings persist when you reboot the system.
          </li>
          <li>
            <label className="label Event">Feedback</label>
            Let's you subscribe to values and get notified when they change. Feedback applies to configuration, status and events.
          </li>
        </ul>
        The best way to understand the xAPI is probably to log in to a device from the command line, and use auto completion to discover the APIs.
      </div>
    </content>
  );
}

module.exports = XapiBasics;
