const React = require('react');

const ValueSpace = require('./ValueSpace');

const CommandParams = (props) => {
  const { params, multiline } = props;

  const list = params.map((param) => {
    const { name, required, valuespace, description } = param;
    const { type } = valuespace;
    const def = param.default;
    const defaultValue = def ? `Default: ${def}` : '';
    const values = <ValueSpace valuespace={valuespace} />;

    const req = required ? <span className="required">Required</span> : '';
    const info = description && (
      <div className="description">{description}</div>
    );

    return (
      <div key={name} className="param">
        <div className="name">{name}</div>
        <div>{req} {type} {values} {defaultValue} {info}</div>
      </div>
    );
  });
  const multi = multiline && <div className="note">This is a multi-line command. Raw data can be sent as an unnamed parameter.</div>;
  const noParams = !params.length && <i>No parameters</i>;
  return (
    <div>
      <div className="params">
        {list}
        {noParams}
      </div>
      {multi}
    </div>
  );
};

module.exports = CommandParams;
