const React = require('react');

const { NavLink } = require('react-router-dom');

const SearchField = require('./SearchField');

const Logo = () => {
  return (
    <a href="#" className="logo"></a>
  );
};

const FeedbackButton = (props) => {
  const { onClick } = props;
  return (
    <button onClick={onClick} className="primary">Feedback</button>
  );
}

const Header = (props) => {
  const { showDialog } = props.actions;
  const isPath = (match, { pathname } ) => (
    pathname.startsWith('/path') || pathname.startsWith('/search')
  );
  const isGuides = (match, { pathname }) => pathname.startsWith('/article');
  const showFeedback = () => showDialog('feedback');

  return (
    <header>
      <Logo />
      <div className="links">
        <NavLink to="/article" isActive={isGuides} activeClassName="selected">Guides</NavLink>
        <NavLink to="/path" isActive={isPath} activeClassName="selected">xAPI</NavLink>
        {/* <NavLink to="/blog">Blog</NavLink> */}
        <NavLink className="support" to="/support" activeClassName="selected">Support</NavLink>
      </div>
      <SearchField />
      <FeedbackButton onClick={showFeedback} />
    </header>
  );
};

module.exports = Header;
