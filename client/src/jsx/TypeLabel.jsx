const React = require('react');

const TypeLabel = (props) => {
  const { type } = props;
  const name = type === 'Configuration' ? 'Config' : type;
  return <label className={"label " + type}>{name}</label>;
};

module.exports = TypeLabel;
