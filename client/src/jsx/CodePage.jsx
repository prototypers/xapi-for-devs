const React = require('react');

const Switcher = require('./Switcher');
const codeHelper = require('../codeHelper');

const Code = (props) => {
  const { code } = props;
  const copy = () => {
    const el = document.querySelector('#copy-paste');
    el.value = code;
    el.select();
    document.execCommand('copy');
    document.getSelection().removeAllRanges();
  }

  return (
    <div className="code">
      <code><pre>{code}</pre></code>
      <button onClick={copy} className="copy">Copy</button>
    </div>
  );
};

const Snippet = (props) => {
  const { snippet } = props;
  return (
    <div>
      <div>{snippet.name}</div>
      {
        snippet.code &&
          <Code code={snippet.code} />
      }
    </div>
  )
}

const CodePage = (props) => {
  const { node, format, setFormat } = props;

  const generators = {
    js: codeHelper.getJsCode,
    tshell: codeHelper.getTshell,
    cloud: codeHelper.getCloud,
  };

  const generator = generators[format] || generators['js'];
  const snippets = generator(node);
  const codes = snippets.map(s => <Snippet key={s.name} snippet={s} />);
  const formats = [
    { key: 'js', value: 'JavaScript' },
    { key: 'tshell', value: 'Command line' },
    { key: 'cloud', value: 'Webex API' },
  ];

  const urlCloud = {
    Status: 'https://developer.webex.com/docs/api/v1/xapi/query-status',
    Command: 'https://developer.webex.com/docs/api/v1/xapi/execute-command',
  };

  const tryIt = format === 'cloud' && urlCloud[node.type] && (
    <div>
      <a href={urlCloud[node.type]} target="_blank">
      Try it live on developer.webex.com</a>
    </div>
  );

  return (
    <div className="api-code">
      <Switcher values={formats} value={format} onChange={setFormat} />
      {codes}
      {tryIt}
    </div>
  );
};

module.exports = CodePage;
