const React = require('react');

const { NavLink, Route, useLocation } = require('react-router-dom');

const Link = (props) => {
  const { name, url, className = "" } = props;

  return (
    <NavLink
      to={url}
      className={className}
      activeClassName="selected">
        {name}
    </NavLink>
  );
};

const GuideMenu = () => {
  return (
    <menu>
      <h1>Guides</h1>
      <NavLink to="/article/Introduction" activeClassName="selected">Introduction</NavLink>
      <NavLink to="/article/CommandLine" activeClassName="selected">Command line</NavLink>
      <NavLink to="/article/Macro/Introduction" activeClassName="selected">Macros</NavLink>
      <Route path="/article/Macro/:article">
        <Link url="/article/Macro/Introduction" name="Introduction" className="sub-menu" />
        <Link url="/article/Macro/Tutorial" name="Tutorial" className="sub-menu" />
      </Route>
      <NavLink to="/article/JSXAPI" activeClassName="selected">JSXAPI</NavLink>
      <NavLink to="/article/XapiToJs" activeClassName="selected">xAPI to JS</NavLink>
      <NavLink to="/article/UiExtensions" activeClassName="selected">UI Extensions</NavLink>
      <NavLink to="/article/WebEngine" activeClassName="selected">Web Engine</NavLink>
      <NavLink to="/article/Resources" activeClassName="selected">Resources</NavLink>
    </menu>
  );
}

const Menu = (props) => {
  const location  = useLocation();
  const showApi = location.pathname.startsWith('/path') || location.pathname.startsWith('/search');

  if (!showApi) {
    return <GuideMenu />;
  }

  const { domains } = props;
  domains.sort((d1, d2) => d1 < d2 ? -1 : 1);
  const pathList = domains.map((path) => {
    const url = '/path/' + path;
    return <NavLink key={path} to={url} activeClassName="selected">{path}</NavLink>;
  });

  return (
    <menu>
      <h1>xAPI</h1>
      {pathList}
    </menu>
  );
};


module.exports = Menu;
