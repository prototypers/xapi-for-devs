const React = require('react');
const { NavLink } = require('react-router-dom');

const SearchField = require('./SearchField');

const HeaderMobile = (props) => {
  const isPath = (match, { pathname } ) => (
    pathname.startsWith('/path') || pathname.startsWith('/search')
  );
  const isGuides = (match, { pathname }) => pathname.startsWith('/article');
  return (
    <header>
      <NavLink to="/article" isActive={isGuides} activeClassName="selected">Guides</NavLink>
      <NavLink to="/path" isActive={isPath} activeClassName="selected">xAPI</NavLink>
      <SearchField />
    </header>
  );
};

module.exports = HeaderMobile;
