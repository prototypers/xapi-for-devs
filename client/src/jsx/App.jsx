
const React = require('react');

const Router = require('./Router');
const schemaHelper = require('../schemaHelper');
const productHelper = require('../productHelper');
const listHelper = require('../listHelper');

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nodes: [],
      allNodes: [],
      domains: [],
      schemas: [],
      schema: null,
      products: [],
      product: 'all',
      options: { format: 'js', fullTextSearch: true },
      dialog: null,
      filters: [],
      internal: false,
    };
    this.actions = {
      setSchema: this.setSchema.bind(this),
      setProduct: this.setProduct.bind(this),
      setOption: this.setOption.bind(this),
      showDialog: this.showDialog.bind(this),
    };

    window.actions = this.actions; // for debugging from console
    window.app = this;
  }

  componentDidMount() {
    this.getSchemas();
  }

  getSchemas() {
    fetch('./schemas/schemas.json')
      .then(r => r.json())
      .then((schemas) => {
        this.setState({ schemas })
        const defaultSchema = schemas[0];
        if (!defaultSchema) {
          alert('Not able to find any xAPI schemas');
        }
        else {
          this.setSchema(defaultSchema.name);
        }
      })
      .catch((e) => {
        console.warn('Failed to read', e);
        alert('Not able to read schema file, sorry');
      });
  }

  setSchema(name) {
    const schema = this.state.schemas.find(s => s.name === name);
    if (!schema) {
      alert('Unable to find schema: ' + name);
      return;
    }
    const url = './schemas/' + schema.url;

    fetch(url)
      .then(r => r.json())
      .then((data) => {
        const nodes = data.objects;
        const domains = schemaHelper.findDomains(nodes);
        const products = schemaHelper.findProducts(nodes);
        // console.log(products);
        const internal = schemaHelper.containsInternal(nodes);
        this.setState({ schema: name, allNodes: nodes, products, internal });
        this.setProduct(this.state.product);
        this.setFilterDefinition();
      })
      .catch((e) => {
        console.error('Failed to read', url, e);
        alert('Not able to read schema file: ' + name);
      });
  }

  setFilterDefinition() {
    const { product, products, schemas, internal } = this.state;
    const schemaNames = schemas.map(s => s.name);
    const productNames = products.map(p => ({ key: p, name: productHelper.productName(p) }))
      .sort((p1, p2) => p1.name < p2.name ? -1 : 1);
    productNames.unshift({ key: 'all', name: 'Any product' });
    const list = values => values.map(v => ({ key: v, name: v }));

    const filters = [];
    filters.push({ name: 'Schema', values: list(schemaNames) });
    filters.push({ name: 'Product', values: productNames });
    filters.push({ name: 'Type', values: listHelper.types });
    if (internal) {
      filters.push({ name: 'Visibility', values: listHelper.visibilities });
    }
    filters.push({ name: 'User', values: listHelper.users });
    filters.push({ name: 'Deployment', values: listHelper.backends });

    this.setState({ filters });
  }

  setProduct(product) {
    const { allNodes } = this.state;
    const nodes = schemaHelper.findProductNodes(allNodes, product);
    const domains = schemaHelper.findDomains(nodes);
    this.setState({ nodes, product, domains });
  }

  setOption(name, value) {
    // console.log('set', name, value);
    if (name === 'Schema') {
      this.setSchema(value);
    }
    else if (name === 'Product') {
      this.setProduct(value);
    }
    else {
      const options = Object.assign(this.state.options, { [name]: value });
      this.setState({ options });
    }
  }

  showDialog(dialog) {
    this.setState({ dialog });
  }

  render() {
    return <Router {...this.state} actions={this.actions} />;
  }
}

module.exports = App;
