const React = require('react');
const marked = require('marked');
const { useParams } = require('react-router-dom');

function loadArticle(file = 'Introduction') {
  const url = `./markdown/${file}.md`;
  // console.log('load', url);

  return fetch(url)
    .then(r => r.text())
    .then((markdown) => {
      const html = marked(markdown);
      return Promise.resolve(html);
    });
}

const Article = () => {
  const { folder, article } = useParams();

  const [{ name, html }, setArticle] = React.useState({ name: 'HomePage', html: 'Loading...' });

  const newName = folder ? folder + article : article;

  // we dont want to parse markdown for every single render, only when article change:
  if (name !== newName) {
    loadArticle(newName)
      .then((html) => setArticle({ name: newName, html }))
      .catch(() => setArticle({ name: newName, html: 'Content not found' }));
  }

  return (
    <content
      key={newName}
      className="article"
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );
}

module.exports = Article;
