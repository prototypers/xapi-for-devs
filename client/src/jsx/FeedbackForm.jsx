const React = require('react');

const Overlay = require('./Overlay');

function saveName(name) {
  localStorage.setItem('feedback_name', name);
}

function loadName() {
  return localStorage.getItem('feedback_name') || '';
}

/* TODO make rest api on server for feedback, move token there */
function sendFeedback(name, text) {
  saveName(name);

  const user = name || 'Anonymous';
  const markdown = `*Feedback from ${name}:* \n${text}`;

  const roomId = '1cffdf90-5a95-11eb-862f-a9be69b8fd1e'; // feedback space
  const token = 'M2VhYzc0ZjEtMTMzOC00N2Q0LWFkNzYtMzk3ZDZkMWQ0MDZiOTgwMTIzMGUtNGE4_PF84_1eb65fdf-9643-417f-9974-ad72cae0e10f'; // feedback bot
  const url = 'https://webexapis.com/v1/messages';
  const body = JSON.stringify({ roomId, markdown });

  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
    body,
  });
}

class FeedbackForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = { mode: 'idle', text: '', name: '' };
  }

  componentDidMount() {
    this.setState({ name: loadName() });
  }

  render() {
    const { text, name, mode } = this.state;
    const close = () => this.props.close();
    const setText = (e) => this.setState({ text: e.currentTarget.value });
    const setName = (e) => this.setState({ name: e.currentTarget.value });

    const submit = () => {
      this.setState({ mode: 'sending' });
      sendFeedback(name, text)
        .then(() => {
          this.setState({ mode: 'sent' });
        })
        .catch(() => {
          this.setState({ mode: 'error' });
        });
    };
    const disabled = mode === 'sending';
    const done = mode === 'sent';

    if (done) {
      return (
        <Overlay onClose={close}>
          <div className="dialog feedback">
            Feedback sent. Thanks!
            <br/>
            <button className="primary" onClick={close}>Close</button>
          </div>
        </Overlay>
      );
    }
    const errorMsg = mode === 'error' && <div className="error">Sorry, not able to send 🙈</div>
    const canSend = !disabled && text.length > 5;

    return (
      <Overlay onClose={close}>
        <div className="dialog feedback">
          Something missing or not working? Ideas for improvements? Tell us!
          <br />
          <input
            placeholder="Name / email (optional)"
            onChange={setName}
            value={name}
            disabled={disabled}
          />
          <br />
          <textarea
            placeholder="Bugs, ideas, feedback"
            value={text}
            onChange={setText}
            disabled={disabled}
          />
          <br />
          { errorMsg }
          <br />
          <button className="primary" disabled={!canSend} onClick={submit}>Submit</button>
          <button onClick={close}>Close</button>
        </div>
      </Overlay>
    );
  }

};

module.exports = FeedbackForm;
