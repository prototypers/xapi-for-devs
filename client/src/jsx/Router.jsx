const React = require('react');
const Media = require('react-media').default;

const {
  HashRouter,
  Switch,
  Redirect,
  Route,
} = require('react-router-dom');

const Header = require('./Header');
const HeaderMobile = require('./HeaderMobile');
const ListView = require('./ListView');
const Menu = require('./Menu');
const Article = require('./Article');
const FeedbackForm = require('./FeedbackForm');
const Filters = require('./Filters');
const XapiBasics = require('./XapiBasics');
const Support = require('./Support');

const msgPrototype = `This site is a prototype. We hope give it a proper, permanent domain soon. Please give feedback!`;

const Router = (props) => {
  return (
    <React.Fragment>
      <Media query="(orientation:landscape)">
        <RouterFullSize {...props} />
      </Media>
      <Media query="(orientation:portrait)">
        <RouterSmallScreen {...props} />
      </Media>
    </React.Fragment>
  );
}

const RouterSmallScreen = (props) => {
  const { domains, actions, nodes, options, filterSettings, internal } = props;
  const { setOption } = actions;

  return (
    <div className="screen mobile">
      <HashRouter>
        <HeaderMobile actions={actions} />
        <Switch>
          <Route exact path="/">
            <Redirect to="/article/" />
          </Route>
          <Route path={["/article/:folder/:article", "/article/:article"]}>
            <Article />
          </Route>
          <Route path={['/path', '/article']} exact>
            <Menu domains={domains} />
          </Route>
          <Route path="/path/:id">
              <ListView
                nodes={nodes}
                options={options}
                setOption={setOption}
                filterSettings={filterSettings}
                internal={internal}
            />
          </Route>
          <Route path="/search/:keyword">
              <ListView
                nodes={nodes}
                options={options}
                setOption={setOption}
                search={true}
                filterSettings={filterSettings}
              />
          </Route>
        </Switch>
      </HashRouter>
    </div>
  );
};

const RouterFullSize = (props) => {
  const {
    domains, nodes, actions, options, dialog, filters, product, schema, internal,
  } = props;
  const { setOption, showDialog } = actions;
  const filterSettings = (
    <Filters
      filters={filters}
      setOption={setOption}
      options={options}
      product={product}
      schema={schema}
    />
  );

  return (
    <HashRouter>
      <div className="screen">
        <Header actions={actions} />
        <Menu domains={domains} />

        <Switch>
          <Route exact path="/">
            <Redirect to="/article/" />
          </Route>
          <Route exact path="/article/">
            <Redirect to="/article/Introduction" />
          </Route>
          <Route path={["/article/:folder/:article", "/article/:article", "/article"]}>
            <Article />
          </Route>
          <Route path="/path/:id">
              <ListView
                nodes={nodes}
                options={options}
                setOption={setOption}
                filterSettings={filterSettings}
                internal={internal}
              />
          </Route>
          <Route path="/support">
            <Support />
          </Route>
          <Route path="/path">
            <XapiBasics />
          </Route>
          <Route path="/search/:keyword">
              <ListView
                nodes={nodes}
                options={options}
                setOption={setOption}
                search={true}
                filterSettings={filterSettings}
              />
          </Route>
        </Switch>

        <div className="banner">
          <div className="bar" title={msgPrototype}>Prototype</div>
        </div>
        { dialog === 'feedback' && <FeedbackForm close={() => showDialog(false)} /> }
      </div>
    </HashRouter>
  );
};


module.exports = Router;
