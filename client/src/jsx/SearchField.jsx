const React = require('react');

import { useHistory } from "react-router-dom";

const SearchField = (props) => {
  const history = useHistory();

  const onKey = (e) => {
    if (e.key === 'Enter' || e.key === 'Return') {
      const word = e.target.value;
      e.target.blur();
      history.push(`/search/${word}`);
    }
  }

  return (
    <div className="search-field">
      <input placeholder="Search xAPI" onKeyUp={onKey} />
    </div>
  );
};

module.exports = SearchField;
