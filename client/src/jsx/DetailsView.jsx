const React = require('react');
const { useParams, useLocation, useHistory, Link } = require('react-router-dom');

const TypeLabel = require('./TypeLabel');
const CommandParams = require('./CommandParams');
const ValueSpace = require('./ValueSpace');
const CodePage = require('./CodePage');
const { productName, simplifyNames } = require('../productHelper');

function concat(word, length) {
  return word.length > length ? word.slice(0, length) + '…' : word;
}

const VersionChooser = (props) => {
  const { versions, current } = props;

  const options = versions.map((v) => {
    // const names = v.products.map(p => productName(p)).join(', ');
    const prods = simplifyNames(v.products);
    const productNames = prods.length > 3
      ? prods.slice(0, 3).join(', ') + ' +' + (prods.length - 3)
      : prods.join(', ');

    return (
      <option key={v.id} value={v.id}>{productNames}</option>
    );
  });

  const location = useLocation();
  const history = useHistory();

  const loadVersion = (e) => {
    const id = e.target.value;
    const path = location.pathname.split('/');
    path.pop();
    path.push(id);
    const newPath = path.join('/');
    history.push(newPath);
  }

  const select = (
    <select value={current} onChange={loadVersion}>{options}</select>
  );

  return (
    <div>Other products: {select}</div>
  );
};

const PropTable = (props) => {
  const rows = props.rows.map((prop) => {
    const { name, value } = prop;
    // const val = concat(value, 50);
    // const val = simplifyNames(value).join(', ');

    return (
      <tr key={name}>
        <td>{name}</td>
        <td title={value}>{value}</td>
      </tr>
    );
  });

  return (
    <table className="props">
      <tbody>
        {rows}
      </tbody>
    </table>
  );
}


const DetailsView = (props) => {
  const { nodes, options, setOption, internal } = props;

  const match = useParams('/path/:path/:id');
  const id = match.node;

  const node = nodes.find(n => n.id == id);
  if (!node) {
    return <span>...</span>;
  }
  const { type, path, attributes, products } = node;
  const { access, backend, role, description, params, multiline, valuespace } = attributes;

  console.debug('Show node:', node);

  let specific;
  if (type === 'Command') {
    specific = <CommandParams params={params} multiline={multiline} />;
  }
  else if (type === 'Configuration' || type === 'Status') {

    const rows = [
      { name: 'Value space', value: <ValueSpace valuespace={valuespace} /> },
    ];
    if (attributes.default) {
      rows.push({ name: 'Default value', value: attributes.default });
    }

    specific = <PropTable rows={rows} />;
  }

  // const productNames = products.map(p => productName(p)).join(', ');
  const productNames = simplifyNames(products).join(', ');
  // console.log(products);
  // console.log(simplifyNames(products));

  const accessNames = {
    'public-api': 'Public',
    'public-api-preview': 'Preview',
    'internal': 'Internal',
  };
  const backends = { any: 'Any', cloud: 'Cloud', onprem: 'On-prem' };
  const commons = [];
  if (internal) {
    commons.push({ name: 'Access', value: accessNames[access] || access });
  }
  commons.push({ name: 'Back-end', value: backends[backend] });
  commons.push({ name: 'User roles', value: role.join(', ') });
  commons.push({ name: 'Products', value: productNames });

  const common = <PropTable rows={commons} />;

  const versions = nodes.filter(n => n.path === node.path && n.type === node.type);
  const versionChooser = versions.length > 1
    && <VersionChooser versions={versions} current={node.id} />;

  const setFormat = f => setOption('format', f);

  return (
    <div className="node-details">

      <div className="api-details">
        <h2>
          <TypeLabel type={type} />
          {path}
        </h2>
        <div className="info">{description}</div>
        {specific}
        {common}
        {versionChooser}
        <textarea id="copy-paste" />
      </div>
      <hr/>
      <CodePage node={node} format={options.format} setFormat={setFormat} />
    </div>
  );
};

module.exports = DetailsView;
