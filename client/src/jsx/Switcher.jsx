const React = require('react');

const Switcher = (props) => {
  const { values, value, onChange } = props;
  const options = values.map(v => <option key={v.key} value={v.key}>{v.value}</option>);
  const change = e => onChange(e.target.value);

  return (
    <select className="switcher" value={value} onChange={change}>{options}</select>
  );
};

module.exports = Switcher;
