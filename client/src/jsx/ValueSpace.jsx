const React = require('react');

const ValueSpace = (props) => {
  const { type, Values, MinLength, MaxLength, Max, Min } = props.valuespace;
  let values = type || '';
  if (MinLength || MaxLength) values = `${MinLength} - ${MaxLength}`;
  else if (Min || Max) values = `${Min} - ${Max}`;
  else if (type === 'Literal') values = Values.join(', ');
  return values;
};

module.exports = ValueSpace;
