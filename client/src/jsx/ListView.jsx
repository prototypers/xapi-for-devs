const React = require('react');
const { useParams, useRouteMatch, NavLink, Route } = require('react-router-dom');

const { findNodesInDomain, filter, textSearch, removeVariants } = require('../schemaHelper');
const TypeLabel = require('./TypeLabel');
const DetailsView = require('./DetailsView');

function parsePath(path) {
  const parents = path.split(' ');
  const name = parents.pop();
  return { name, root: parents[0] || name, path: parents.join(' ') };
}

function sortAndGroupNodes(nodes) {
  nodes.sort((p1, p2) => p1.path < p2.path ? -1 : 1);

  const tree = {};
  nodes.forEach((node) => {
    const { name, path } = parsePath(node.path);

    // This effectively merges the list sections to one section instead of one per list index
    // Connector[1-3], Connector[n], ... => Connector[]
    const pathSimple = path.replace(/\[[0-9\.\-n]*\]/g, '[…]');
    if (!tree[pathSimple]) {
      tree[pathSimple] = [];
    }
    tree[pathSimple].push(node);
  });

  const list = [];
  for (key in tree) {
    list.push({ name: key, nodes: tree[key] });
  }
  list.sort((i1, i2) => i1.name < i2.name ? -1 : 1);

  return list;
}

const ListViewHandler = (props) => {
  const { id, keyword } = useParams();
  const pathId = id;
  const { nodes, options, setOption, search } = props;
  const hasDetails = useRouteMatch('/path/:path/:node');
  let fullscreen = !hasDetails;
  const { fullTextSearch } = options;

  let result;

  // Show search result (doesnt care about domains)
  if (search) {
    // We really shouldnt do this in a view render function
    // but its hard to move it to the store due to the way the router shortcircuits everything

    const filtered = filter(nodes, options);
    const startTime = new Date().getTime();
    result = removeVariants(textSearch(filtered, keyword, fullTextSearch));
    const time = new Date().getTime() - startTime;
    console.debug('Search time (ms):', time);
  }
  // Browse domain (Cameras, Video, UserInterface etc):
  else {
    matchingNodes = findNodesInDomain(nodes, pathId);
    result = removeVariants(filter(matchingNodes, options));
  }

  const list = sortAndGroupNodes(result);
  const count = { matching: result.length, total: nodes.length };

  return (
    <ListView
      {...props}
      key={id || 'search'}
      list={list}
      search={keyword}
      nodes={nodes}
      fullscreen={fullscreen}
      count={count}
    />
  );
}

const ListView = (props) => {
  const {
    options, setOption, list, fullscreen, nodes, filterSettings, search, count, internal,
  } = props;


  const content = list.map((group) => {
    const nodes = group.nodes.map((node) => {
      // const { id, name, pathId } = node;
      const { path = '', type, id, attributes = {} } = node;
      const { access } = attributes;
      let symbol = '';
      if (access === 'public-api-preview') symbol = '🔥';
      else if (access === 'internal') symbol = '🔒';
      const { name, root } = parsePath(path);
      const url = search
        ? `/search/${search}/${id}`
        : `/path/${root}/${id}`;

      return (
        <div key={id} className="node-path">
          <TypeLabel type={type} />
          <NavLink activeClassName="selected" to={url}>{path}</NavLink> {symbol}
        </div>
      );
    });

    return (
      <div key={group.name}>
        <section>{group.name}</section>
        {nodes}
      </div>
    );
  });

  const { fullTextSearch } = options;
  const onFullText = e => setOption('fullTextSearch', e.target.checked);

  const textSearchOption = (
    <div>
      <input type="checkbox" id="fulltext" checked={fullTextSearch} onChange={onFullText} />
      <label htmlFor="fulltext">Full text search (descriptions, parameters, etc)</label>
    </div>
  );
  const listStyle = fullscreen ? { width: '100%' } : {};
  const urlPath = search ? '/search/:keyword/:node' : '/path/:path/:node';

  return (
    <content className="main">
      <div className="api-list" style={listStyle}>
        {search && textSearchOption}
        Items: <b>{count.matching}</b> (of {count.total})
        {content}
      </div>
      <Route path={urlPath} children={
        <DetailsView
          nodes={nodes}
          options={options}
          setOption={setOption}
          internal={internal}
        />
      } />
      {filterSettings}
    </content>
  );
};


module.exports = ListViewHandler;
