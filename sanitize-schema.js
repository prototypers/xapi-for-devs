const fs = require('fs');
const path = require('path');

const products = require('./client/src/productHelper').names;

if (process.argv.length < 4) {
  console.log(`Usage: node ${process.argv[1]} sourceFolder targetFolder`);
  process.exit(1);
}
const source = process.argv[2];
const target = process.argv[3];

const files = fs.readdirSync(source);
files.forEach((file) => {
  if (file.endsWith('.json')) {
    const input = path.join(source, file);
    const output = path.join(target, file);
    sanitize(input, output);
  }
})


function sanitize(input, output) {
  console.log(`Sanitizing ${input} to ${output}`);

  const data = JSON.parse(fs.readFileSync(input));

  const objects = data.objects.filter((node) => {
    return node.attributes.access === 'public-api';
  });

  // filter away unwanted products (typically not public yet)
  objects.forEach((node) => {
    const allowed = node.products.filter((p) => {
      return !!products[p];
    });
    node.products = allowed;
  });

  const json = JSON.stringify({ objects }, null, 2);
  fs.writeFileSync(output, json);
}
