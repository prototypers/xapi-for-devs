#!/bin/bash

# Builds the software
# Generates public version of the api schema
# Copies the scheme to public and private folders
# Copies the complete package to internal and external web server

echo "Building..."
npm run build

echo "Creating public site..."
cp -R markdown build/

# echo "Copying schemas..."
# cp schemas/schemas-public.json build/public/schemas.json
# cp schemas/schemas-private.json build/private/schemas.json
# cp schemas/private/*.json build/private/

# echo "Removing private nodes..."
# node sanitize-schema ./schemas/public build/public/

# echo "Copying to public server..."
scp -r build/*.* tbjolset@custom-collab.cisco.com:~/public_html/xapi/
scp -r build/markdown tbjolset@custom-collab.cisco.com:~/public_html/xapi/

echo "Copying to private server"
scp -r build/*.* prototypes.gen@prototypes.cisco.com:~/hosted/xapi/
scp -r build/markdown prototypes.gen@prototypes.cisco.com:~/hosted/xapi/
